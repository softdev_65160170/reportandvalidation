/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dsi.reportproject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Surap
 */
public class ArtistDao implements Dao<Artist>{


    @Override
    public Artist get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Artist> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Artist save(Artist obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Artist insert(Artist obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Artist update(Artist obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Artist obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Artist> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    public List<ArtistReport> getArtistByTotalPrice(int limit){
        ArrayList<ArtistReport> list = new ArrayList();
        String sql = """
                     SELECT EMP.EMP_ID,EMP.EMP_NAME,SUM(RD.RD_QUANTITY) as TotalQuantity
                         ,SUM(RD.RD_PRICE*RD.RD_QUANTITY) as TotalPrice FROM EMPLOYEE EMP
                     INNER JOIN RECEIPT RC ON RC.EMP_ID = EMP.EMP_ID
                     INNER JOIN RECEIPTDETAIL RD ON RD.R_ID = RC.R_ID
                     INNER JOIN PRODUCT PD ON PD.PD_ID = RD.PD_ID
                     
                     GROUP BY EMP.EMP_ID
                     ORDER BY TotalPrice DESC
                     LIMIT ?""";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ArtistReport artist = ArtistReport.fromRS(rs);
                list.add(artist);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
