/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.dsi.reportproject;

import java.util.List;

/**
 *
 * @author BAM
 */
public interface Dao<T> {
    T get(int id);
    List<T> getAll();
    T save(T obj);
    T insert(T obj);
    T update(T obj);
    int delete(T obj);
    List<T> getAll(String where, String order);
}
